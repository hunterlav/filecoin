use openssl::sha::sha256;
use rand::{thread_rng, Rng};
use std::str;
use std::sync::mpsc;

pub fn mine(hexcode: &str, state_machine: &[usize], sender: mpsc::Sender<(usize, String, String)>) {
    // Preallocate everything
    // 57 bytes = 76 bytes in base 64
    let mut arr = [0u8; 57];
    let mut b64 = [0u8; 76];
    // sha256 string is always 64 characters in length
    let mut hash_string = [0u8; 64];
    let bytes = hexcode.as_bytes();
    let final_state = state_machine.len() / 16 - 1;

    let mut hash_count = 0;
    loop {
        thread_rng().fill(&mut arr[..]);
        base64::encode_config_slice(&arr, base64::STANDARD, &mut b64);

        // Use combined search and encode to only loop over bytes once
        if search_and_hex(
            bytes,
            final_state,
            &state_machine,
            &sha256(&b64),
            &mut hash_string,
        ) {
            sender
                .send((
                    hash_count,
                    // we know the string is utf8, that's what base64 outputs
                    unsafe { str::from_utf8_unchecked(&b64).to_owned() },
                    unsafe { str::from_utf8_unchecked(&hash_string).to_owned() },
                ))
                .unwrap();
            hash_count = 0;
        }
        hash_count += 1
    }
}

fn search_and_hex(
    hexcode: &[u8],
    final_state: usize,
    state_machine: &[usize],
    hash: &[u8; 32],
    hash_string: &mut [u8; 64],
) -> bool {
    let mut state: usize = 0;
    let mut upper: usize;
    let mut lower: usize;

    // loop through each byte, using the lower and upper bits to do a search
    // in the state machine and to encode the hex string
    // I feel like this can still be optimized somehow
    for n in hash.iter().enumerate() {
        upper = (n.1 >> 4) as usize;
        lower = (n.1 & 0x0f) as usize;

        state = state_machine[16 * state + upper];
        state = state_machine[16 * state + lower];

        hash_string[n.0 * 2] = hexcode[upper];
        hash_string[(n.0 * 2) + 1] = hexcode[lower];
    }

    state == final_state
}
