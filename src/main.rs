use std::fs::OpenOptions;
use std::io::Write;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use std::time;

extern crate clap;
use clap::{App, Arg};

mod miner;

fn main() {
    let (hexcode, path, pattern) = parse_args();

    // Atomic counters so the stats thread can read the value
    let counter = Arc::new(AtomicUsize::new(0));
    let hashes = Arc::new(AtomicUsize::new(0));

    // Create thread pool & mpsc channel
    let mut threads = Vec::new();
    let (sender, receiver) = mpsc::channel();

    let now = time::Instant::now();

    for _ in 0..8 {
        // Clone the mpsc channel for each individual thread
        let s = sender.clone();
        let h = hexcode.clone();
        let p = pattern.clone();
        threads.push(thread::spawn(move || {
            miner::mine(&h, p.as_slice(), s);
        }));
    }

    // Open file in append mode
    let mut file = match OpenOptions::new().append(true).open(path) {
        Err(e) => {
            eprintln!("Couldn't open file: {}", e);
            std::process::exit(0);
        }
        Ok(f) => f,
    };

    // Setup stat printing thread
    let c = counter.clone();
    let h = hashes.clone();
    thread::spawn(move || loop {
        thread::sleep(time::Duration::from_secs(1));
        let count = c.load(Ordering::Relaxed);
        let hash_count = h.load(Ordering::Relaxed);
        println!(
            "
{} Coins minted
{:.1} Coins/s
{} Hashes
{:.1} Mh/s",
            count,
            count as f64 / now.elapsed().as_millis() as f64 * 1000.,
            hash_count,
            (hash_count as f64 / now.elapsed().as_millis() as f64 * 1000.) / 1000000.
        );
    });

    // Main thread, consolidates the mined coins from the worker threads and
    // appends them to the wallet
    loop {
        // Wait for coin to be mined
        match receiver.recv() {
            Ok((hash_count, b64, hash)) => {
                // Acquire & Release ordering because we're loading
                // and storing at the same time
                counter.fetch_add(1, Ordering::AcqRel);
                hashes.fetch_add(hash_count, Ordering::AcqRel);
                writeln!(&mut file, "{}:{}", b64, hash).unwrap();
            }
            Err(e) => eprintln!("MPSC Error: {}", e),
        }
    }
}

fn parse_args() -> (String, String, Vec<usize>) {
    let matches = App::new("File Coin")
        .version("0.1")
        .about("Mints coins by finding the pattern in the hash")
        .arg(
            Arg::with_name("hexcode")
                .validator(|s| {
                    if s.len() != 16 {
                        return Err(String::from("Hex code must be 16 characters in length"));
                    }
                    Ok(())
                })
                .short("x")
                .long("hexcode")
                .value_name("hexcode")
                .help("replacement for the standard hex character set eg. 0123456789abcdef")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pattern")
                .required(true)
                .short("p")
                .long("pattern")
                .value_name("PATTERN")
                .help("Pattern to find in the resulting hash")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("wallet")
                .required(true)
                .short("w")
                .long("wallet")
                .value_name("wallet")
                .help("Path to the wallet that holds the coins")
                .takes_value(true),
        )
        .get_matches();

    let hexcode = matches
        .value_of("hexcode")
        .unwrap_or("0123456789abcdef")
        .to_owned();
    let path = matches.value_of("wallet").unwrap().to_owned();
    let pattern = compile_state_machine(matches.value_of("pattern").unwrap(), &hexcode);
    (hexcode, path, pattern)
}

fn compile_state_machine(pattern: &str, hexcode: &str) -> Vec<usize> {
    let mut count = 0;
    let mut machine: Vec<usize> = Vec::new();
    for c in pattern.chars() {
        count += 1;
        let mut empty = [0; 16];
        let location = hexcode
            .find(c)
            .expect("Pattern does not contain characters found in hex character set");
        empty[location] = count;

        machine.extend(&empty);
    }
    machine.extend([count; 16]);
    machine
}

#[test]
fn test_compile_state_machine() {
    let pattern = "deadbeef";
    let hexcode = "0123456789abcdef";
    let machine = vec![
        //   0 1 2 3 4 5 6 7 8 9 a b c d e f
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
    ];
    let iter = machine.iter();

    let result = compile_state_machine(pattern, hexcode);

    let count = result.iter().zip(iter).filter(|&(x, y)| x != y).count();

    assert!(count == 0);
}
